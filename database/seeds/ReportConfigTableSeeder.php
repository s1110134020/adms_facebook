<?php

use Illuminate\Database\Seeder;

use App\ReportConfig;

class ReportConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mainColumnsConfig = '{"columns": [
            {"field": "campaignName" ,"visible": "true", "sortable":"true", "title": "campaign Name<br>廣告名稱"},
            {"field": "reach" ,"visible": "true", "sortable":"true", "title": "reach<br>觸及人數"},
            {"field": "impressions" ,"visible": "true", "sortable":"true", "title": "impressions<br>曝光"},
            {"field": "ctr" ,"visible": "true", "sortable":"true", "title": "CTR<br>轉換率"},
            {"field": "cpc" ,"visible": "true", "sortable":"true", "title": "CPC<br>點擊費用"},
            {"field": "spend" ,"visible": "true", "sortable":"true", "title": "spend<br>花費"},
            {"field": "date_start" ,"visible": "true", "sortable":"true", "title": "date start<br>開始時間"},
            {"field": "date_stop" ,"visible": "true", "sortable":"true", "title": "date stop<br>結束時間"},
            {"field": "relevance_score" ,"visible": "true", "sortable":"true", "title": "relevance score<br>分數"},
            {"field": "like" ,"visible": "true", "sortable":"true", "title": "like<br>按讚數"},
            {"field": "post_reaction" ,"visible": "true", "sortable":"true", "title": "post reaction<br>貼文互動"},
            {"field": "video_view" ,"visible": "true", "sortable":"true", "title": "video view<br>影片觀看"},
            {"field": "link_click" ,"visible": "true", "sortable":"true", "title": "link click<br>連結點擊數"},
            {"field": "post" ,"visible": "true", "sortable":"true", "title": "post<br>貼文分享次數"},
            {"field": "page_engagement" ,"visible": "true", "sortable":"true", "title": "page engagement"},
            {"field": "post_engagement" ,"visible": "true", "sortable":"true", "title": "post_engagement"},
            {"field": "cost_per_like" ,"visible": "true", "sortable":"true", "title": "cost per like<br>按讚費用"},
            {"field": "cost_per_post_reaction" ,"visible": "true", "sortable":"true", "title": "cost per post reaction<br>貼文互動費用"},
            {"field": "cost_per_video_view" ,"visible": "true", "sortable":"true", "title": "cost per video view<br>影片觀看費用"},
            {"field": "cost_per_link_click" ,"visible": "true", "sortable":"true", "title": "cost per link click<br>連結點擊費用"},
            {"field": "cost_per_post" ,"visible": "true", "sortable":"true", "title": "cost per post<br>貼文分享費用"},
            {"field": "cost_per_page_engagement" ,"visible": "true", "sortable":"true", "title": "cost per page engagement<br>粉絲頁互動費用"},
            {"field": "cost_per_post_engagement" ,"visible": "true", "sortable":"true", "title": "cost per post engagement<br>貼文互動費用"},
            {"field": "avg_time_video_view" ,"visible": "true", "sortable":"true", "title": "avg time video view<br>平均影片觀看"},
            {"field": "30_sec_video_view" ,"visible": "true", "sortable":"true", "title": "30 sec video view<br>30 秒影片觀看"},
            {"field": "p25_video_view" ,"visible": "true", "sortable":"true", "title": "25％ video view<br>25% 影片觀看"},
            {"field": "p50_video_view" ,"visible": "true", "sortable":"true", "title": "50％ video view<br>50% 影片觀看"},
            {"field": "p75_video_view" ,"visible": "true", "sortable":"true", "title": "75％ video view<br>75% 影片觀看"},
            {"field": "p95_video_view" ,"visible": "true", "sortable":"true", "title": "95％ video view<br>95% 影片觀看"},
            {"field": "p100_video_view" ,"visible": "true", "sortable":"true", "title": "100％ video view<br>100% 影片觀看"}
        ]}'; 
        // $report_config_base = 
        

        $columnsConfig = json_decode($mainColumnsConfig);
        foreach ($columnsConfig -> columns as $column) {
            if($column -> field == "campaignName")
                $column -> visible = "false";
        }

        $main_report_config = new ReportConfig();
        $main_report_config -> name = 'report01';
        $main_report_config -> title = '貼文互動';
        $main_report_config -> columns_config = json_encode($columnsConfig);
        $result = $main_report_config -> save();
        $result = ReportConfig::find(1);
        // error_log($result -> columns_config);
        $ary = json_decode($result -> columns_config);
        error_log($ary -> columns[0] -> field);
        error_log($ary -> columns[0] -> visible);
        

    }
}
