<?php

namespace App\Services\ADMS;

use Exception;

class FacebookService
{

    public function apiRequest($api_url)
    {

        try {

            $response = app('FB') -> get($api_url);
            
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            throw e;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            throw e;
        }

        return json_decode($response -> getBody());
    }

    public function getMe()
    {
        try {
            $api_url = '/me';
            return $this -> apiRequest($api_url);  
        } catch (Exception $e) {
            throw e;
        } 
    }

    public function getAdAccounts($meId)
    {    

        try {
            $api_url = '/' . $meId . "/adaccounts?fields=name";
            return $this -> apiRequest($api_url);
        } catch (Exception $e) {
            throw e;
        } 

    }

    public function getAdsInsights($adaccountId, $query)
    {
        try {
            $api_url = '/' . $adaccountId .'/'. $query;
            return $this -> apiRequest($api_url);
        } catch (Exception $e) {
            throw e;
        } 

    }

    public function getInsightsQuery($time_range = null, $time_increment = null)
    {
        $fields = "campaign_name,cost_per_action_type,video_avg_time_watched_actions,video_30_sec_watched_actions,video_p25_watched_actions,video_p50_watched_actions,video_p75_watched_actions,video_p95_watched_actions,video_p100_watched_actions, reach, impressions, clicks, ctr, cpc, actions, spend, relevance_score";
        $query = "insights?level=ad&fields=" . $fields;        
        
        if($time_range){
            $since = $time_range["start"];
            $until = $time_range["end"];
            $query = $query . "&time_range={'since':'$since','until':'$until'}";
        }

        if($time_increment){
            $query = $query . "&time_increment=$time_increment";
        }

        
        return $query;

    }

    public function getInsightsSummaryQuery($time_range = null)
    {
        
        $query = $this -> getInsightsQuery($time_range);
        return $query;
    }

    public function getInsightsDailyQuery($time_range = null)
    {
        $time_increment = 1;
        $query = $this -> getInsightsQuery($time_range, $time_increment);
        return $query;
    }


    public function parseAdsInsightsToTable($adsInsightsData)
    {
        $result = array();
        try {
            
            foreach($adsInsightsData -> data as $key => $data){
                $rowData = (object)[];
                $rowData -> campaignName = $data -> campaign_name;
                


                $rowData -> reach = $data -> reach;
                $rowData -> clicks = $data -> clicks;
                $rowData -> impressions = $data -> impressions;
                $rowData -> ctr = $data -> ctr;
                $rowData -> cpc = $data -> cpc;
                $rowData -> spend = $data -> spend;
                $rowData -> date_start = $data -> date_start;
                $rowData -> date_stop = $data -> date_stop;
                $rowData -> relevance_score = $data -> relevance_score -> score;
                if (array_key_exists('actions', $data)) {
                    foreach ($data -> actions as $item) {
                        $actionType = $item -> action_type;
                        $rowData -> $actionType = $item -> value;
                    }
                }

                if (array_key_exists('cost_per_action_type', $data)) {
                    foreach ($data -> cost_per_action_type as $item) {
                        $field = "cost_per_" . $item -> action_type;
                        $rowData -> $field = $item -> value;
                    }
                }

                if (array_key_exists('video_avg_time_watched_actions', $data)) {
                    foreach ($data -> video_avg_time_watched_actions as $item) {
                        $field = "avg_time_" . $item -> action_type;
                        $rowData -> $field = $item -> value;
                    }
                }
                if (array_key_exists('video_30_sec_watched_actions', $data)) {
                    foreach ($data -> video_30_sec_watched_actions as $item) {
                        $field = "30_sec_" . $item -> action_type;
                        $rowData -> $field = $item -> value;
                    }
                }
                if (array_key_exists('video_p25_watched_actions', $data)) {
                    foreach ($data -> video_p25_watched_actions as $item) {
                        $field = "p25_" . $item -> action_type;
                        $rowData -> $field = $item -> value;
                    }
                }
                if (array_key_exists('video_p50_watched_actions', $data)) {
                    foreach ($data -> video_p50_watched_actions as $item) {
                        $field = "p50_" . $item -> action_type;
                        $rowData -> $field = $item -> value;
                    }
                }
                if (array_key_exists('video_p75_watched_actions', $data)) {
                    foreach ($data -> video_p75_watched_actions as $item) {
                        $field = "p75_" . $item -> action_type;
                        $rowData -> $field = $item -> value;
                    }
                }
                if (array_key_exists('video_p95_watched_actions', $data)) {
                    foreach ($data -> video_p95_watched_actions as $item) {
                        $field = "p95_" . $item -> action_type;
                        $rowData -> $field = $item -> value;
                    }
                }
                if (array_key_exists('video_p100_watched_actions', $data)) {
                    foreach ($data -> video_p100_watched_actions as $item) {
                        $field = "p100_" . $item -> action_type;
                        $rowData -> $field = $item -> value;
                    }
                }





                array_push($result, $rowData);
                
            }
            
        } catch (Exception $e) {
            throw $e;

        }

        return $result;
    }


}
