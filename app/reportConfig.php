<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reportConfig extends Model
{
    //

    protected $casts = [
        'columns_config' => 'json',
    ];
}
