<?php

namespace App\Http\Controllers\ADMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request as data_request;

use App\Services\ADMS\FacebookService;
use Exception;

use View;
use Auth;
use Redirect;
use Illuminate\Http\Request;

class FacebookController extends Controller
{
    protected $facebookService;

    public function __construct(FacebookService $facebookService)
    {
        $this-> facebookService = $facebookService;
    }

    public function index()
    {
        if (Auth::check()) {
            return View::make('adms/facebook');
        } else {
            return Redirect::action('AuthController@login');
        }
    }

    public function findAll(Request $request)
    {   

        $time_range = $request -> input('time_range');
        $query = $this-> facebookService -> getInsightsSummaryQuery($time_range);
        $data = $this-> facebookService -> getAdsInsights("act_951554451632279", $query);
        $result = $this-> facebookService -> parseAdsInsightsToTable($data);

        return response()->json($result);
    }

}
