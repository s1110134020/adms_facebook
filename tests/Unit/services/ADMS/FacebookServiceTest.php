<?php

namespace Tests\Unit\services\ADMS;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Services\ADMS\FacebookService;

class FacebookServiceTest extends TestCase
{   

    protected $facebookService;

    public function setup()
    {
        
        $this->facebookService = new FacebookService();
        parent::setUp();
    }

    public function testGetMe()
    {
        try {
            $result = $this-> facebookService -> getMe();

            echo $result -> id; // 116105052310093

        } catch (Exception $e) {
            throw $e;
        }
    }
    // public function testGetBusinessProjects()
    // {
    //     try {
    //         $result = $this-> facebookService -> getBusinessProjects("619604941493900");

            

    //         foreach($result -> data as $businessProject){
                
    //             echo $businessProject -> name . " = " . $businessProject -> id . "\xA" ;
    //         }

    //     } catch (Exception $e) {
    //         throw $e;
    //     }
    // }

    public function testGetAdAdaccounts()
    {
        try {
            $result = $this-> facebookService -> getAdaccounts("116105052310093");

            

            foreach($result -> data as $adAccounts){
                
                echo $adAccounts -> name . " = " . $adAccounts -> id . "\xA" ;
            }

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function testGetAdsInsights()
    {
        try {
            $query = $this-> facebookService -> getInsightsQuery();
            $result = $this-> facebookService -> getAdsInsights("act_951554451632279", $query);

            echo json_encode($result);

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function testGetAdsInsightsByRangeAndDay()
    {
        try {
            $time_range = [
                'start' => '2017-07-04',
                'end' =>'2017-08-02'
            ];
            $time_increment = 1;
            $query = $this-> facebookService -> getInsightsQuery($time_range, $time_increment);
            $result = $this-> facebookService -> getAdsInsights("act_951554451632279", $query);

            echo json_encode($result);

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function testParseAdsInsightsToTable()
    {
        try {
            $query = $this-> facebookService -> getInsightsSummaryQuery();
            $data = $this-> facebookService -> getAdsInsights("act_951554451632279", $query);
            $result = $this-> facebookService -> parseAdsInsightsToTable($data);
            echo "\xA" . "\xA";
            echo json_encode($result);

        } catch (Exception $e) {
            throw $e;
        }
    }

    
}
