<?php

namespace Tests\Unit\services;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Services\Admin\UserService;

class UserServiceTest extends TestCase
{   

    protected $userService;

    public function setup()
    {
        
        $this->userService = new UserService();
        parent::setUp();
    }

    public function testUserCreate()
    {
        try {
            $user['id'] = "";
            $user["email"] = "test@gmail.com";
            $user["name"] = "test user";
            $result = $this-> userService -> save($user);


            $this->assertEquals($result["email"], $user["email"]);

        } catch (Exception $e) {
            throw $e;
        }
    }
}
