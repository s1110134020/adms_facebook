@extends('layouts.admin')

@section('title','Facebook')
@section('page_title','報表查詢')

@section('content')

@php ($REST_API = '/api/adms/facebook/')
        <div class="content" id="panel-list">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <header>
                            <div class="row">
                                <div class="col-md-8 col-xs-8">
                                    <h3>典華</h3>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <div class="btn-group bootstrap-select">
                                        <div class="btn-group bootstrap-select">
                                             <select name="type" class="selectpicker" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" tabindex="-98">
                                                <option class="bs-title-option" value="dh">典華</option>
                                                <option value="id">Bahasa Indonesia</option>
                                                <option value="ms">Bahasa Melayu</option>
                                                <option value="ca">Català</option>
                                            </select> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </header>

                            <hr>

                            <div class="select">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div id="reportrange" class="pull-right btn bs-caret mgbottom">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                            <span></span><b class="caret pull-right"></b>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="btn-group bootstrap-select mgbottom">
                                             <select name="type" class="selectpicker" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" tabindex="-98">
                                                <option class="bs-title-option" value="">貼文互動</option>
                                                    <option value="id">轉換-All</option>
                                                    <option value="ms">轉換-完成購買</option>
                                                    <option value="ca">轉換-註冊</option>
                                                    <option value="da">行動應用程式安裝</option>
                                                    <option value="da">影片觀看</option>
                                                    <option value="da">加入粉絲專頁</option>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-wd btn-success mgbottom boldline">
                                            <span class="btn-label">
                                                <i class="fa fa-external-link"></i>
                                            </span>
                                            &nbsp;&nbsp;匯出報表
                                        </button>
                                    </div>
                                </div>
                            </div>
                            

                            <ul role="tablist" class="nav nav-tabs">
                                <li role="presentation" class="active" style="margin-left: 30px;">
                                    <a href="#agency" data-toggle="tab">Campaign</a>
                                </li>
                                <li>
                                    <a href="#company" data-toggle="tab">Ad Sets</a>
                                </li>
                                <li>
                                    <a href="#style" data-toggle="tab">Ads</a>
                                </li>
                                <li>
                            </ul>
                            
                            <div class="content table-responsive table-full-width">
                                <div class="toolbar">
                                    
                                </div>
                                
                                <table id="bootstrap-table" class="table" data-toggle="table" data-url="{{$REST_API}}" data-click-to-select="ture">
                                </table>

                                <div class="clearfix"></div>

                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>



@stop

@section('script')

<script type="text/javascript">
    var csrf_token = $('meta[name="csrf-token"]').attr('content');

    var __REST_API_URL__ = '{{$REST_API}}';
    
    var panelView = new Vue({
        el: '#panel-view',
        data: {
            row: {
                title: "Undefined"
            },
            time_range: {
                'start': '',
                'end':''
            }
        },
        mounted() {
            var that = this;
            var start = moment().subtract(30, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
                that.time_range.start = start.format('YYYY-MM-DD');
                that.time_range.end = end.format('YYYY-MM-DD');

            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                "alwaysShowCalendars": true,
                ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb)
            .on("apply.daterangepicker", function(ev, picker) {
                that.time_range.start = picker.startDate.format('YYYY-MM-DD');
                that.time_range.end = picker.endDate.format('YYYY-MM-DD');
                $table.bootstrapTable('refresh');
            });

            cb(start, end);



            var initDataTable = function($table) {
                var columns = [
                    {"field": "campaignName" ,"visible": "true", "sortable":"true", "title": "campaign Name<br>廣告名稱"},
                    {"field": "reach" ,"visible": "true", "sortable":"true", "title": "reach<br>觸及人數"},
                    {"field": "impressions" ,"visible": "true", "sortable":"true", "title": "impressions<br>曝光"},
                    {"field": "ctr" ,"visible": "true", "sortable":"true", "title": "CTR<br>轉換率"},
                    {"field": "cpc" ,"visible": "true", "sortable":"true", "title": "CPC<br>點擊費用"},
                    {"field": "spend" ,"visible": "true", "sortable":"true", "title": "spend<br>花費"},
                    {"field": "date_start" ,"visible": "true", "sortable":"true", "title": "date start<br>開始時間"},
                    {"field": "date_stop" ,"visible": "true", "sortable":"true", "title": "date stop<br>結束時間"},
                    {"field": "relevance_score" ,"visible": "true", "sortable":"true", "title": "relevance score<br>分數"},

                    {"field": "like" ,"visible": "true", "sortable":"true", "title": "like<br>按讚數"},
                    {"field": "post_reaction" ,"visible": "true", "sortable":"true", "title": "post reaction<br>貼文互動"},
                    {"field": "video_view" ,"visible": "true", "sortable":"true", "title": "video view<br>影片觀看"},
                    {"field": "link_click" ,"visible": "true", "sortable":"true", "title": "link click<br>連結點擊數"},
                    {"field": "post" ,"visible": "true", "sortable":"true", "title": "post<br>貼文分享次數"},
                    {"field": "page_engagement" ,"visible": "true", "sortable":"true", "title": "page engagement"},
                    {"field": "post_engagement" ,"visible": "true", "sortable":"true", "title": "post_engagement"},

                    {"field": "cost_per_like" ,"visible": "true", "sortable":"true", "title": "cost per like<br>按讚費用"},
                    {"field": "cost_per_post_reaction" ,"visible": "true", "sortable":"true", "title": "cost per post reaction<br>貼文互動費用"},
                    {"field": "cost_per_video_view" ,"visible": "true", "sortable":"true", "title": "cost per video view<br>影片觀看費用"},
                    {"field": "cost_per_link_click" ,"visible": "true", "sortable":"true", "title": "cost per link click<br>連結點擊費用"},
                    {"field": "cost_per_post" ,"visible": "true", "sortable":"true", "title": "cost per post<br>貼文分享費用"},
                    {"field": "cost_per_page_engagement" ,"visible": "true", "sortable":"true", "title": "cost per page engagement<br>粉絲頁互動費用"},
                    {"field": "cost_per_post_engagement" ,"visible": "true", "sortable":"true", "title": "cost per post engagement<br>貼文互動費用"},


                    {"field": "avg_time_video_view" ,"visible": "true", "sortable":"true", "title": "avg time video view<br>平均影片觀看"},
                    {"field": "30_sec_video_view" ,"visible": "true", "sortable":"true", "title": "30 sec video view<br>30 秒影片觀看"},
                    {"field": "p25_video_view" ,"visible": "true", "sortable":"true", "title": "25％ video view<br>25% 影片觀看"},
                    {"field": "p50_video_view" ,"visible": "true", "sortable":"true", "title": "50％ video view<br>50% 影片觀看"},
                    {"field": "p75_video_view" ,"visible": "true", "sortable":"true", "title": "75％ video view<br>75% 影片觀看"},
                    {"field": "p95_video_view" ,"visible": "true", "sortable":"true", "title": "95％ video view<br>95% 影片觀看"},
                    {"field": "p100_video_view" ,"visible": "true", "sortable":"true", "title": "100％ video view<br>100% 影片觀看"},
                ];
                
                $table.bootstrapTable({
                    toolbar: ".toolbar",
                    striped: true,
                    sortOrder: 'desc',
                    sortName: 'updatedAt',
                    clickToSelect: true,
                    showRefresh: true,
                    search: true,
                    showToggle: false,
                    showColumns: true,
                    pagination: true,
                    columns: columns,
                    searchAlign: 'right',
                    pageSize: 8,
                    clickToSelect: false,
                    pageList: [8, 10, 25, 50, 100],
                    formatShowingRows: function(pageFrom, pageTo, totalRows){
                        return "共 " + totalRows + " 筆 ";
                    },
                    formatRecordsPerPage: function(pageNumber){
                        return "每頁顯示 " + pageNumber + " 筆資料";
                    },
                    icons: {
                        refresh: 'fa fa-refresh',
                        toggle: 'fa fa-th-list',
                        columns: 'fa fa-columns',
                        detailOpen: 'fa fa-plus-circle',
                        detailClose: 'fa fa-minus-circle'
                    },
                    queryParams: function (p) {
                        return {
                            time_range: that.time_range
                        };
                    },
                });
                $(window).resize(function () {
                    $table.bootstrapTable('resetView');
                });
            };

            var $table = $('#bootstrap-table');
            
            initDataTable($table);

            

            // $table.bootstrapTable('getVisibleColumns').forEach(function(column){
            //     $table.bootstrapTable('hideColumn', column.field);
            // });

            
        },
        methods: {
            done: function(e) {
                if (e) e.preventDefault();
                $('#panel-view').hide();
                $('#panel-list').show();
            },
            load: function(id) {
                _this = this;
                Vue.http.get(__REST_API_URL__ + id).then(function(response) {
                    _this.row = response.body;
                    Vue.http.get(__REST_API_URL__ + id + '/roles').then(function(response) {
                        _this.row.roles = response.body;
                    });
                });
            }
        }
    });







</script>


@stop



