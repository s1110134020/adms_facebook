<!doctype html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8" />
	<link rel="icon" type="image/png" href="/admin/assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Light Bootstrap Dashboard PRO by Creative Tim</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Bootstrap core CSS     -->
    <link href="/admin/assets/css/bootstrap.min.css" rel="stylesheet" />
        
    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="/admin/assets/css/app.css" rel="stylesheet">

    <!-- facebook日期套件Include Required Prerequisites -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <!-- facebook的CSS -->
    <link rel="stylesheet" href="/admin/assets/css/facebook.css"> 
    
        
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="/admin/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    @show
    
    
    <style type="text/css">
    .sidebar .sidebar-wrapper {
        height: calc(100vh - 64px);
    }
    .logo img {
        max-width: 180px;
        padding:50px 0;
    }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="/admin/assets/img/full-screen-image-3.jpg">


            <div class="logo" style="border-bottom:1px solid rgba(255, 255, 255, 0.2);">
                <a href="#" class="logo-text">
                    ADMS
                </a>
            </div>
            <div class="logo logo-mini">
                <a href="＃" class="logo-text">
                    ADMS
                </a>
            </div>

            <div class="sidebar-wrapper">
                
                <ul class="nav">

                    <li>
                        <a href="dashboard.html">
                            <i class="pe-7s-graph1"></i>
                            <p>主控台</p>
                        </a>
                    </li>

                    <li class="{{ Request::is('admin/user') || Request::is('admin/role') ? 'active' : '' }}">
                        <a data-toggle="collapse" href="#UserManagement">
                            <i class="pe-7s-users"></i>
                            <p>後台帳號管理
                            <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{ Request::is('admin/user') || Request::is('admin/role') ? 'in' : '' }}" id="UserManagement">
                            <ul class="nav">
                                <li class="{{ Request::is('admin/user') ? 'active' : '' }}"><a href="/admin/user">後台帳號管理</a></li>
                                <li class="{{ Request::is('admin/role') ? 'active' : '' }}"><a href="/admin/role">後台角色管理</a></li>
                            </ul>
                        </div>
                    </li>


                    <li>
                        <a href="dashboard.html">
                            <i class="pe-7s-note2"></i>
                            <p>專案管理</p>
                        </a>
                    </li>

                    <li class="{{ Request::is('adms/*') ? 'active' : '' }}">
                        <a data-toggle="collapse" href="#report">
                            <i class="pe-7s-news-paper"></i>
                            <p>報表查詢
                            <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse {{ Request::is('adms/*') ? 'in' : '' }}" id="report">
                            <ul class="nav">
                                <li class="{{ Request::is('adms/facebook') ? 'active' : '' }}"><a href="/adms/facebook">Facebook</a></li>
                                <li class="{{ Request::is('adms/google') ? 'active' : '' }}"><a href="/adms/google">Google</a></li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="dashboard.html">
                            <i class="pe-7s-config"></i>
                            <p>組態管理</p>
                        </a>
                    </li>


                </ul>

            </div>
        </div>

        <div class="main-panel">

			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-minimize">
						<button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
							<i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
							<i class="fa fa-navicon visible-on-sidebar-mini"></i>
						</button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">@yield('page_title')</a>
					</div>
					<div class="collapse navbar-collapse">

						

						<ul class="nav navbar-nav navbar-right">

							<li class="dropdown dropdown-with-icons">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-user"></i>
									<p>
										{{ Auth::user()->username }}
										<b class="caret"></b>
									</p>
								</a>
								<ul class="dropdown-menu dropdown-with-icons">
									<li>
										<a href="#">
											<i class="pe-7s-lock"></i> 修改帳號資料
										</a>
									</li>
									<li>
										<a href="logout" class="text-danger swal-prompt">
											<i class="pe-7s-close-circle"></i>
											登出
										</a>
									</li>
								</ul>
							</li>

						</ul>
					</div>
				</div>
			</nav>

            <div class="content">
                @yield('content')
            </div>

            <footer class="footer">
                <div class="container-fluid">
                    <p class="copyright pull-right">
                        Copyright &copy; 2017 ADMS
                    </p>
                </div>
            </footer>
        </div>
    </div>
    
</body>
    
    <!--   Vue.js   -->
    <script src="/lib/vue/dist/vue.min.js"></script>
    <script src="/lib/vue-resource/dist/vue-resource.min.js"></script>
    <script src="/lib/vee-validate/dist/vee-validate.min.js"></script>
    <script src="/lib/vee-validate/dist/locale/zh_TW.js"></script>
    <script src="/lib/moment/min/moment.min.js"></script>

    

    <!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
    <script src="/admin/assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="/admin/assets/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="/admin/assets/js/bootstrap.min.js" type="text/javascript"></script>


	<!--  Forms Validations Plugin -->
	<script src="/admin/assets/js/jquery.validate.min.js"></script>

	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="/admin/assets/js/moment.min.js"></script>

    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="/admin/assets/js/bootstrap-datetimepicker.js"></script>

    <!--  Select Picker Plugin -->
    <script src="/admin/assets/js/bootstrap-selectpicker.js"></script>

	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="/admin/assets/js/bootstrap-checkbox-radio-switch-tags.js"></script>

	<!--  Charts Plugin -->
	<script src="/admin/assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="/admin/assets/js/bootstrap-notify.js"></script>

    <!-- Sweet Alert 2 plugin -->
	<script src="/admin/assets/js/sweetalert2.js"></script>

    <!-- Vector Map plugin -->
	<script src="/admin/assets/js/jquery-jvectormap.js"></script>

    <!--  Google Maps Plugin    -->
    <script src="https://maps.googleapis.com/maps/api/js"></script>

	<!-- Wizard Plugin    -->
    <script src="/admin/assets/js/jquery.bootstrap.wizard.min.js"></script>

    <!--  Bootstrap Table Plugin    -->
    <script src="/admin/assets/js/bootstrap-table.js"></script>

	<!--  Plugin for DataTables.net  -->
    <script src="/admin/assets/js/jquery.datatables.js"></script>


    <!--  Full Calendar Plugin    -->
    <script src="/admin/assets/js/fullcalendar.min.js"></script>

    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="/admin/assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<script src="/admin/assets/js/demo.js"></script>
    <!-- facebook日期套件Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <!-- <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script> -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <!-- Scripts -->
     <!-- <script src="{{ asset('js/app.js') }}"></script>  -->
     <!-- <script src="/js/app.js"></script>  -->

    <!-- Dashboard開啟通知 -->
     <!-- <script type="text/javascript">
    	$(document).ready(function(){

        	demo.initDashboardPageCharts();
        	demo.initVectorMap();

        	$.notify({
            	icon: 'pe-7s-bell',
            	message: "<b>Light Bootstrap Dashboard PRO</b> - forget about boring dashboards."

            },{
                type: 'warning',
                timer: 4000
            });

    	});
    </script>  -->
    
    <script type="text/javascript">

        var notifyAfterHttpSuccess = function(response) {
            if (!response) return;
            var body = response.body || response;
            if (body && body.message) {
                $.notify({
                    message: body.message
                }, {
                    type: body.type || (body.result?'success':'danger'),
                    timer: 1500
                });
            }
        };

        var notifyAfterHttpError = function(response) {
            var message = response?'操作失敗(代碼: ' + response.status + ' - ' + response.statusText + ')':'操作失敗';
            $.notify({
                message: message
            }, {
                type: 'danger',
                timer: 2000
            });
        };

        var getUrlParameter = function(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        Vue.use(VeeValidate, {locale: 'zh_TW'}); // good to go. 

        // Setup Vue Filters
        Vue.filter('formatDate', function(value) {
            if (!value) return value;
            return moment(String(value)).format('YYYY/MM/DD HH:mm:ss');
        });

        Vue.filter('formatBasename', function(value) {
            if (!value) return value;
            return value.split('/').pop();
        });

        Vue.filter('showdown', function(value) {
            if (!value) return value;
            return showdown.makeHtml(value);
        });
	</script>
    @yield('script')
</html>
